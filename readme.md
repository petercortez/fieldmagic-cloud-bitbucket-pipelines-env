# BitBucket Pipelines environment for Fieldmagic Cloud

Allows us to test our Lumen API just like how BitBucket Pipelines would do it.

### Installation
```shell
$ cp .envsample .env
$ docker-compose build --no-cache
```
Set the following variables because they'll be read by the *docker-compose.yml* file:

* `DB_NAME`
* `DB_USERNAME`
* `DB_PASSWORD`
* `FM_CLOUD_SERVER_DIR`
* `FM_CLOUD_CLIENT_DIR`

If you make any changes to the environment variables, make sure to build the container again.
### Usage

#### **Server**
Before running the commands below, make sure that you have configured your Docker client to have atleast 8GB of memory allowed for use because just like in our BitBucket Pipelines configuration, we allocate atleast 8GB.
```shell
$ docker-compose up -d
$ docker exec -it fmc-pipeline-server /bin/bash
$ composer install
$ vendor/bin/phpunit
```
#### **Client**

```shell
$ docker-compose up -d
$ docker exec -it fmc-pipeline-client /bin/bash
$ npm install
$ npm rebuild
$ npm run test
```
The _client_ container will have a custom node_modules directory mounted onto it, ensuring that we are required to run
`npm install` when we first start it up. Also, when switching between branches, it is crucial that we have a fresh installation of all packages via the following commands:
```shell
$ rm -rf node_modules/*
$ npm install
$ npm rebuild
```
The package-lock.json has also been mounted with an empty json file so that it isn't read -- just like how it is in our BitBucket Pipeline.
